This manifest contains files to setup multiple repositories in proper structure using `wstool`.

## **Prerequisite** ##


You will need wstool, check if it is already installed  or to install it:

```sudo apt-get install python-wstool```

-----------------------------------

# Setup repository using wstool
*************************

Experimental Script
--------------------
```
#!bash

$ wget https://bitbucket.org/auro_repo/auro_manifest/raw/master/scripts/auro_repo_clone.sh
$ sh auro_repo_clone.sh 
```

The codebase is in multi repo structure. The structure is mentioned in yaml files

 *  auro_full.yaml :Everything
 *  auro_pc.yaml :For development computers, no drivers for sensors and actuators
 *  auro_vehicle.yaml : For vehicle or shuttle, no simulation, no sandbox

Choose the yaml you want for the following steps and replace "auro_full.yaml" in the commands where needed


- Generate key for SSH based login

```
#!bash

ssh-keygen -t rsa
ssh-add ~/.ssh/id_rsa
ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
```
- Set key in Bitbucket account ```avatar > Manage account > SSH keys > Add Key```

` wstool init auro_repo https://bitbucket.org/auro_repo/auro_manifest/raw/master/auro_full.yaml `



##_Other Alternatively_ for using HTTPS if you cannot use SSH##

It is important to save you credentials to use multi-repo configuration
Temporary solution
```git config --global credential.helper 'cache --timeout 3600'``` (3600 means 1 hour)

OR


- Create a file called `.netrc` in your home directory (`~/.netrc`).

- Add credentials with using the following format:

```
#!bash

machine bitbucket.org
login myusername
password mypassword
```

```
#!bash
git clone https://bitbucket.org/auro_repo/auro_manifest.git
./auro_manifest/set_https.sh  auro_manifest/auro_full.yaml
wstool init auro_repo auro_manifest/auro_full.yaml

```
-----------------------------------

# Should know
_____________

## Using wstool/git/svn ##

Later in the individual repos you can use git/svn command as relevant.

OR you can use:

```
wstool update

wstool info
```


## GIT commands to use ##


**For `git push`:**

```
git add .

git commit -m "message"

git stash  # optional

git pull

git push origin master

git stash pop
```

Learn more at [https://bitbucket.org/auro_repo/auro_docs/wiki/Learnning%20GIT](https://bitbucket.org/auro_repo/auro_docs/wiki/Learnning%20GIT)
