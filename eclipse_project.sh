#!/usr/bin/env bash
echo "Generating eclipse projects for all ROS projects in this directory"
ROOT=$PWD
for PROJECT in `find $PWD -name .project`; do
    DIR=`dirname $PROJECT`
    echo $DIR
    cd $DIR
    awk -f $(rospack find mk)/eclipse.awk .project > .project_with_env && mv .project_with_env .project
done
cd $ROOT
