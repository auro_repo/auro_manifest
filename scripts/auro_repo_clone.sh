#!/bin/bash

# Taking clone location from user
echo -n "Enter the Clone Location: "
read location
cd $location 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then
   echo "Current Location is" $location
else
   echo "Please enter a valid location"
   exit 0
fi

# Removing existing auro_repo code from the given location
ls -lrt $location/auro_repo 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then
   echo
   echo "Removing existing auro_repo code from" $location
   rm -rf $location/auro_repo
fi 


# Cloneing auro_repo
echo
echo " **** Cloneing auro_repo in" $location "****"
ssh-add ~/.ssh/id_rsa
ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
wstool init auro_repo https://bitbucket.org/auro_repo/auro_manifest/raw/master/auro_full.yaml -j8 #1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then
   echo
   echo "Clone completed successfully" 
fi

# Copy basic setup from "/home/priyanka/auro_repo/manifest/home" location

cp $location/auro_repo/manifest/home/make-completion-wrapper.sh $HOME/make-completion-wrapper.sh 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then
   echo
   echo "'make-completion-wrapper.sh' copied successfully" 
else
   echo
   echo "Please manually copy the file" $location/auro_repo/manifest/home/make-completion-wrapper.sh "to" $HOME "location" 
fi

cp -R $location/auro_repo/manifest/home/bash-git-prompt $HOME/bash-git-prompt
if [ $? -eq 0 ]
then
   echo
   echo "'bash-git-prompt' directory structure copied successfully" 
else
   echo
   echo "Please manually copy the directory structure from" $location/auro_repo/manifest/home/bash-git-prompt "to" $HOME "location" 
fi




# Primary code configuration

echo
echo " **** Primary code configuration ****"
cd $location/auro_repo/primary_code 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then
   echo
   echo "Current Location is" $location/auro_repo/primary_code
   catkin config --link-devel -llog_ct -bbuild_ct -ddevel_ct -iinstall_ct --extend /opt/ros/indigo 1>/dev/null 2>/dev/null
   if [ $? -eq 0 ]
   then
      
      # Primary code catkin build
      echo
      echo "Run Catkin Build for primary code"
      catkin build -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8  --env-cache 1>/dev/null 2>/dev/null
      if [ $? -eq 0 ]
      then
         echo "Catkin Build successful for auro_repo primary code"
      else
         echo "Catkin Build unsuccessful for auro_repo primary code. Please do it manually"
         echo "Syntax: catkin build -G\"Eclipse CDT4 - Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8  --env-cache" 
      fi
                    
      # Primary code catkin make
      echo
      echo "Run Catkin Make for primary code"
      catkin_make --build build_cm -DCATKIN_DEVEL_PREFIX=devel_cm -DCMAKE_BUILD_TYPE=Debug 1>/dev/null 2>/dev/null
      if [ $? -eq 0 ]
      then
         echo "Catkin Make successful for auro_repo primary code"
      else
         echo "Catkin make unsuccessful for auro_repo primary code. Please do it manually"
         echo "Syntax: catkin_make --build build_cm -DCATKIN_DEVEL_PREFIX=devel_cm -DCMAKE_BUILD_TYPE=Debug" 
      fi
   else
      echo "Primary code configuration failed. Plese run the below syntax manually"
      echo "Syntax 1: catkin config --link-devel -llog_ct -bbuild_ct -ddevel_ct -iinstall_ct --extend /opt/ros/indigo"
      echo "Syntax 2: catkin build -G\"Eclipse CDT4 - Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8  --env-cache"
      echo "Syntax 3: catkin_make --build build_cm -DCATKIN_DEVEL_PREFIX=devel_cm -DCMAKE_BUILD_TYPE=Debug"
   fi
fi

# Sandbox code configuration

echo
echo "**** sandbox code configuration ****"
cd $location/auro_repo/sandbox 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]
then
   echo
   echo "Current Location is" $location/auro_repo/sandbox
   catkin config --link-devel -llog_ct -bbuild_ct -ddevel_ct -iinstall_ct --extend ../primary_code/devel_ct 1>/dev/null 2>/dev/null
   if [ $? -eq 0 ]
   then
      
      # Sanbox code catkin build
      echo
      echo "Run Catkin Build for sandbox code"
      catkin build -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8  --env-cache 1>/dev/null 2>/dev/null 1>/dev/null 2>/dev/null
      if [ $? -eq 0 ]
      then
         echo "Catkin Build successful for auro_repo sandbox code"
      else
         echo "Catkin Build unsuccessful for auro_repo sandbox code. Please do it manually"
         echo "Syntax: catkin build -G\"Eclipse CDT4 - Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8  --env-cache 1>/dev/null 2>/dev/null" 
      fi
      
      # Sanbox code catkin make              
      echo
      echo "Run Catkin Make for sandbox code"
      catkin_make --build build_cm -DCATKIN_DEVEL_PREFIX=devel_cm -DCMAKE_BUILD_TYPE=Debug 1>/dev/null 2>/dev/null
      if [ $? -eq 0 ]
      then
         echo "Catkin Make successful for auro_repo sandbox code"
      else
         echo "Catkin make unsuccessful for auro_repo sandbox code. Please do it manually"
         echo "Syntax: catkin_make --build build_cm -DCATKIN_DEVEL_PREFIX=devel_cm -DCMAKE_BUILD_TYPE=Debug" 
      fi
   else
      echo "Sandbox code configuration failed. Plese run the below syntax manually"
      echo "Syntax 1: catkin config --link-devel -llog_ct -bbuild_ct -ddevel_ct -iinstall_ct --extend ../primary_code/devel_ct"
      echo "Syntax 2: catkin build -G\"Eclipse CDT4 - Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8  --env-cache"
      echo "Syntax 3: catkin_make --build build_cm -DCATKIN_DEVEL_PREFIX=devel_cm -DCMAKE_BUILD_TYPE=Debug"
   fi
fi
