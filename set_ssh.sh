#!/bin/bash
filename=`echo "$1" | cut -d'.' -f1`
sed 's/https:\/\/bitbucket.org\/auro_repo/git@bitbucket.org:auro_repo/g'  $1 > "$filename"_ssh.yaml
